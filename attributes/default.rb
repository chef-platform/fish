#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'fish'

# Configure fish repository
default[cookbook_name]['repository'] = {
  'name' => 'Fish shell - 2.x release series (CentOS_7)',
  'baseurl' => 'http://download.opensuse.org/repositories/shells'\
    ':/fish:/release:/2/CentOS_7/',
  'gpgcheck' => 1,
  'gpgkey' => 'http://download.opensuse.org/repositories/shells'\
    ':/fish:/release:/2/CentOS_7/repodata/repomd.xml.key'
}

# Which package to install
default[cookbook_name]['package'] = 'fish'

# Where to put the config file
default[cookbook_name]['config_file'] = '/etc/fish/config.fish'

# fish configuration (in config_file)
# It is just the array of lines of the config file (like applying lines method)
default[cookbook_name]['config'] = []

# Retries on yum/package resources (mostly for test purpose)
default[cookbook_name]['package_retries'] = nil

#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package_retries = node[cookbook_name]['package_retries']

yum_repository 'shells_fish_release_2' do
  description node[cookbook_name]['repository']['name']
  baseurl node[cookbook_name]['repository']['baseurl']
  gpgkey node[cookbook_name]['repository']['gpgkey']
  gpgcheck node[cookbook_name]['repository']['check']
  retries package_retries unless package_retries.nil?
  action :create
end

package node[cookbook_name]['package'] do
  retries package_retries unless package_retries.nil?
end

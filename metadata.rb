name 'fish'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/fish@'\
  'incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure fish shell'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/fish'
issues_url 'https://gitlab.com/chef-platform/fish/issues'
version '1.1.0'

supports 'centos', '>= 7.5'

chef_version '>= 13.0'
